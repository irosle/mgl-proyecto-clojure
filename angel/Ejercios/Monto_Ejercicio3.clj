(import '(java.util Scanner))
(println  "=> Introduce Monto:")
(def scan (Scanner. *in*))
(def monto(.nextDouble scan))


(if(> monto 100.00) 
(do
	(println (/ 10.00 100.00))
    (println monto)
	(def descuento(* (/ 10.00 100.00) monto))
	(print descuento)
	)  
(do
	(def descuento(* monto (/ 2.00 100.00)))
	(print descuento)
	) 


)


