(import '(java.util Scanner))
(def scan (Scanner. *in*))
(println  "¿Cual es el sueldo?")
(def sueldo(.nextInt scan))
(def sueldo_neto 0)
(def descuento 0)
;procesos para <= 2000
(def operacion (- sueldo 1000))
(def operacion (* operacion 0.05))
(def operacion (+ operacion (* 1000 0.1)))
;/////////////////////////////

;procesos para >2000
(def operacion2 (- sueldo 2000))
(def operacion2 (* operacion2 0.03))
(def operacion2 (+ operacion2 (* 1000 0.1)))
;///////////////////////////////////

(if (<= sueldo 1000)

	(def descuento (* sueldo 0.1))
	
	(if (<= sueldo 2000)
		(def descuento operacion)


		(if (> sueldo 2000)
			(def descuento operacion2)

		)
	)

)

(println descuento)
