(import '(java.util Scanner))
(println "introduce el cantidad ")
(def scan (Scanner. *in*))
(def cantidad(.nextDouble scan))

(if (>= cantidad 100) 
	(def descuento(* cantidad(/ 10 100))))
(if (< cantidad 100)
	(def descuento(* cantidad(/ 2 100))))


(println "El cantidad es: " cantidad)
(println "El descuento es: " descuento)
