
(def matriz [[0 0 0 0 0][0 0 0 0 0] [0 0 0 0 0][0 0 0 0 0][0 0 0 0 0]])

(defn latino [ fila colum contador orden mat]

    (if (and (= fila 0)(= colum 0)) (do  
        (def matAux (assoc (nth matriz 0) 0 1)) 
        (def matriz (assoc matriz 0 matAux))) 

        (do 

            (if (= fila colum) (do 
                (latino (- fila 1) (- orden 1) contador orden mat)) 
                    (do (def matAux (assoc (nth matriz fila) colum contador))
                    (def matriz (assoc matriz fila matAux))
                    (latino fila (- colum 1) (+ contador 1) orden mat))
            )
        )
    
    ) 

)

(latino (count matriz) (count (nth matriz 0)) 0 5 matriz)

(def incremento 0)

(while (< incremento 5)
    (println (nth matriz incremento))
    (def incremento (inc incremento))
)
