(import (java.util Scanner))

(def  entrada (Scanner. *in*)) ;el scanner se llama entrada
	
(println "Ingrese el primer numero: ")
(def num1 (.nextInt entrada))

(println "Ingrese el operador de la operacion deseada")
(def simbolo (.next entrada)) 
	
(println "Ingrese el segundo numero: ")
(def num2 (.nextInt entrada))

	(if (= simbolo "+")
		(println "La suma de" num1 " + " num2 " es: " (+ num1 num2)))
	(if (= simbolo "-")
		(println "La resta de" num1 " - " num2 " es: " (- num1 num2)))
	(if (= simbolo "*")
		(println "La multiplicacion de" num1 " * " num2 " es: " (* num1 num2)))
	(if (= simbolo "/")
		(println "La division de" num1 " / " num2 " es: " (/ num1 num2)))


