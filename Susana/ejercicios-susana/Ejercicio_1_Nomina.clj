(import '(java.util Scanner))
(def teclado (Scanner. *in*))
(def tarifaEx 0)
(def horasEx 0)
(def salario 0)
(println "----NOMINA----")
(println "Introduzca las Horas trabajadas")
(def horasT (.nextInt teclado))
(println "Introduzca la Tarifa")
(def tarifa (.nextInt teclado))

(if (<= horasT 40)

	(def salario (* horasT tarifa))

	(if (> horasT 40)
		( do
		(def sum (+ tarifa 0.50))
		(def tarifaEx (* sum tarifa))
		(def horasEx (- horasT 40 ))
		(def mult (* horasEx horasEx))
		(def mult2 (* 40 tarifa))
		(def salario (+ mult mult2))
		)
		)
	)
(println "El salario es:")
(println salario)




